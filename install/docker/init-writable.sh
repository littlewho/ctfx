#!/bin/sh

mkdir -p /var/www/ctfx/writable
mkdir -p /var/www/ctfx/writable/cache
mkdir -p /var/www/ctfx/writable/upload

chmod -R 777 /var/www/ctfx/writable
